import React from 'react';
import ReactDOM from 'react-dom';
import Papa from 'papaparse';
import 'papaparse/player/player.css';

const Table = props => (
  <BootstrapRow>
    <p className="info">Found {props.filteredCount || 0} record(s)</p>
    <div className="table-responsive">
      <table className="table table-striped table-hover">
        <Head columns={props.columns} handleSortChange={props.handleSortChange} />
        <Body displayData={props.displayData} columns={props.columns} />
      </table>
    </div>
  </BootstrapRow>
);

const Head = props => (
  <thead>
    <tr>
      {
        props.columns.map((column,i) => (
          <th key={i}><a onClick={() => props.handleSortChange(column)}>{column}</a></th>
        ))
      }
    </tr>
  </thead>
);

class Body extends React.Component {
  render() {
    return (
      <tbody>
        {
          this.props.displayData.map((r,i)=> (
            <Row key={i} data={r} columns={this.props.columns} />
          ))
        }
      </tbody>
    );
  }
}

const Row = props  => (
  <tr>
    {
      props.columns.map((h,i) => (
        <td key={i}>{ props.data[h] }</td>
      ))
    }
  </tr>
);

const BootstrapRow = props => (
  <div className="row">
    <div className="col-xs-12">
      {props.children}
    </div>
  </div>
);

class CSVInput extends React.Component {
  handleChange() {
    const file = ReactDOM.findDOMNode(this.refs.csv).files[0];
		if(!file)
			return;
		this.props.handleFileChange(file);
  }
  render() {
    return (
			<BootstrapRow>
				<form className="form-inline">
					<div className="form-group">
						<label htmlFor="file">CSV File</label>
						<input type="file" className="form-control" id="file" accept=".csv" onChange={() => this.handleChange()} ref="csv" />
					</div>
				</form>
			</BootstrapRow>
		);
  }
}

class GenericFilter extends React.Component {
  handleChange(e) {
    const filterValue = ReactDOM.findDOMNode(this.refs.filterValue).value
		const filterColumn = ReactDOM.findDOMNode(this.refs.filterColumn).value
		if(!filterValue || !filterColumn)
			return;
		this.props.handleFilterChange(filterColumn, filterValue);
  }
  render() {
    return (
			<BootstrapRow>
				<form className="form-inline">
					<div className="form-group">
						<label htmlFor="filterColumn">Filter Value</label>
						<select className="form-control" id="filterColumn" defaultValue={this.props.columns[0]} onChange={e => this.handleChange(e)} ref="filterColumn">
							<option value="">Select column</option>
							{
                this.props.columns.map((c,i) => (
                  c !== '' && <option key={i} value={c}>{c}</option>
                ))
              }
						</select>
					</div>
						<div className="form-group">
						<label htmlFor="filterValue">Filter Value</label>
						<input type="text" className="form-control" accept=".csv" id="filterValue" onChange={e => this.handleChange(e)} ref="filterValue" />
					</div>
				</form>
			</BootstrapRow>
		);
  }
}

class Pager extends React.Component {
	handlePaginationChange(page) {
		this.props.handlePaginationChange(page)
	}
  render() {
    if(this.props.filteredCount < 50)
			return null;

		const currentPage = this.props.page;

		const showPages = 9
		const showPagesHalf = Math.floor(showPages / 2.0);
		const totalPages = Math.ceil(this.props.filteredCount / 50.0)
		const startIndex = Math.max(0, (currentPage > (totalPages - showPagesHalf) ? (totalPages - showPages) : (currentPage - showPagesHalf)));

		const pages = []
		var i = startIndex;
		for (; pages.length < Math.min(showPages, totalPages); i++) {
			pages.push({display : i + 1, page : i})
		}

		if(startIndex > 0)
			pages.unshift({display: "...", page: Math.ceil(startIndex / 2.0) })
		if(i < totalPages)
			pages.push({display: "...", page: Math.floor(i + ((totalPages - i) / 2.0))})
		return (
			<BootstrapRow>
				<nav>
					<ul className="pagination">
						<li>
							<a onClick={ () => this.handlePaginationChange(0)}>First</a>
						</li>
						<li>
							<a aria-label="Previous" onClick={() => this.handlePaginationChange("-1")}>
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						{pages.map(function(i){
							var active = currentPage === i.page ? "active" : null;
							return <li className={active} key={i.page}><a onClick={() => this.handlePaginationChange(i.page)}>{i.display}</a></li>
						}.bind(this))}
						<li>
							<a aria-label="Next" onClick={() => this.handlePaginationChange( "+1")}>
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
						<li>
							<a onClick={() => this.handlePaginationChange(totalPages-1)}>Last</a>
						</li>
					</ul>
				</nav>
			</BootstrapRow>
		);
  }
}

class Wrapper extends React.Component {
  onDragEnter(e) {
    e.stopPropagation();
    e.preventDefault();
  }
  onDrop(e) {
    e.stopPropagation();
    e.preventDefault();
		var dt = e.dataTransfer;
		var files = dt.files;

		if(files && files.length && files[0])
			this.props.handleFileChange(files[0]);
  }
  onDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
  }
  render() {
    return (
      <div style={{ height: "100%" }}
        onDrop={e => this.onDrop(e)}
        onDragEnter={e => this.onDragEnter(e)}
        onDragOver={e => this.onDragOver(e)}
        >
        {this.props.children}
      </div>
    );
  }
}

class Interface extends React.Component {
	getFileSelectState(data, state){
		state.data = data.data;
		state.columns = data.meta.fields;
		state.totalCount = data.data.length;

		return this.getFilterChangeState(null, null, state);
	}
	getFilterChangeState(column, value, fileState){
		if(column && value)
		{
			var loweredFilter = value.toLowerCase();
			var filtered = fileState.data.filter(function(d){

				return d[column] && d[column].toString().toLowerCase().indexOf(loweredFilter) > -1
			});
			fileState.filteredData = filtered;
			fileState.filteredCount = filtered.length;
		}
		else
		{
			fileState.filteredData = fileState.data;
			fileState.filteredCount = fileState.totalCount;
		}

		return this.getSortChangeState(null, fileState);
	}
	getSortChangeState(col, filterState) {
    let desc;
		if(!col)
		{
			col = filterState.sort.slice(1);
			desc = filterState.sort[0] === "-";
		}
		else
		{
			desc = filterState.sort.slice(1) === col && filterState.sort[0] !== "-";
		}

		filterState.sortedData = filterState.filteredData.sort(function(a, b) {
			return a[col] === b[col] ? 0 : (a[col] < b[col] ? -1 : 1) * (desc ? -1 : 1);
		});
		filterState.sort = (desc ? "-" : "+") + col;


		return this.getPaginationChangeState(0, filterState);
	}
	getPaginationChangeState(page, sortState) {
		if(typeof page === "string")
		{
			if(page === "-1" && this.state.page > 0)
				page = sortState.page - 1;
			else if(page === "+1" && sortState.page < Math.floor(sortState.filteredCount / 50))
				page = sortState.page + 1;
			else
				page = sortState.page;
		}

		sortState.page = page;
		sortState.displayData = sortState.sortedData.slice(page * 50, (page + 1) * 50);
		return sortState;
	}
	handleDrop(event){
		event.preventDefault();
	}
	handleFileChange(file)
	{
		Papa.parse(file, {
			header: true,
			dynamicTyping: true,
			complete: (data) => this.handleDataChange(data)
		});
	}
	handleDataChange(data){
		this.setState(this.getFileSelectState(data, this.state));
	}
	handleFilterChange(column, value){
		this.setState(this.getFilterChangeState(column, value, this.state));
	}
	handleSortChange(col) {
		this.setState(this.getSortChangeState(col, this.state));
	}
	handlePaginationChange(page){
		this.setState(this.getPaginationChangeState(page, this.state));
	}
	constructor(props){
    super(props);
    this.state = {
      data: [],
      filteredData: [],
      sortedData: [],
      displayData: [],
      columns: [],
      totalCount: 0,
      filteredCount: 0,
      page: 0,
      sort: "+yearID",
    };
	}
  validationCheck() {
    return true;
  }
  sendToServer(){
    if(this.validationCheck(this.state.filteredData)) {
      console.warn("Submitting", this.state.filteredData);
    } else {
      alert('Missing Parameters')
    }
  }
	render() {
		return (
			<Wrapper handleFileChange={(file) => this.handleFileChange(file)}>
				<CSVInput handleFileChange={(file) => this.handleFileChange(file)} data={this.state.data} />
				<GenericFilter handleFilterChange={(col, val) => this.handleFilterChange(col, val)} columns={this.state.columns}/>
				<Table displayData={this.state.displayData} columns={this.state.columns} handleSortChange={(col) => this.handleSortChange(col)} filteredCount={this.state.filteredCount} />
				<Pager filteredCount={this.state.filteredCount} page={this.state.page} handlePaginationChange={(page) => this.handlePaginationChange(page)} />
        {
          this.state.filteredData.length !== 0 &&
          <button onClick={() =>this.sendToServer()}>Submit CSV</button>
        }
			</Wrapper>
		);
	}
}


export default Interface;
