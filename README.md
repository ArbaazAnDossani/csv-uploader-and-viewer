CSV Input and Rendering POC

Features:
User can upload a CSV(Comma Separated Values) file
On upload, the user can view the data in table format
The user can select particular column to trim view
The Table is paginated with 50 rows per page, additional jumps to first and last pages added
Generic Search within the CSV file, update the View
Validation Check for missing Parameters
Service call after validation check to save Users data

Steps to Run:
1. git clone https://ArbaazAnDossani@bitbucket.org/ArbaazAnDossani/csv-uploader-and-viewer.git
2. npm i
3. npm start
